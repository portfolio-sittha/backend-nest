# backend-nest

## deploy with docker
create database

docker build -t portfolio/api-nestjs .

docker container run --rm -t portfolio/api-nestjs

```
type: "mysql",
host: "127.0.0.1",
port: 3306,
username: "root",
password: "",
database: "backend-nest"
```

how to start

1. npm i -g @nestjs/cli
2. yarn
3. yarn start:dev

api info (postman)

[postman](https://www.getpostman.com/collections/27617cf9ccc38c9ce149)