import { Body, Controller, Get, Post, Res } from "@nestjs/common";
import { Response } from "express";
import { RedeemSaveDto } from "./dto/redeem.dto";
import { RedeemService } from "./redeem.service";

@Controller("/redeem")
export class RedeemController {
  constructor(private readonly redeemService: RedeemService) {}

  @Get() async getAll(@Res() res: Response) {
    const result = await this.redeemService.getAll();
    res.send({ result: result });
  }

  @Post() async save(@Body() req: RedeemSaveDto, @Res() res: Response) {
    const result = await this.redeemService.save({ user: req.user, product: req.product });

    res.send({ redeem: result });
  }
}
