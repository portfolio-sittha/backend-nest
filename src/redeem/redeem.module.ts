import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Redeem } from "src/enitites/redeem.enitity.";
import { RedeemController } from "./redeem.controller";
import { RedeemService } from "./redeem.service";
import { Users } from '../enitites/users.enitity';
import { Product } from '../enitites/product.enitity';

@Module({
  imports: [TypeOrmModule.forFeature([Redeem, Users, Product])],
  controllers: [RedeemController],
  providers: [RedeemService],
})
export class RedeemModule {}
