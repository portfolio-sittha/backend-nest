import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Redeem } from "src/enitites/redeem.enitity.";
import { FindOneOptions, Repository } from "typeorm";
import { Users } from "../enitites/users.enitity";
import { Product } from "../enitites/product.enitity";
import { errorMessage } from "src/lib/general";

@Injectable()
export class RedeemService {
  constructor(
    @InjectRepository(Redeem)
    private readonly repoRedeem: Repository<Redeem>,
    @InjectRepository(Users)
    private readonly repoUser: Repository<Users>,
    @InjectRepository(Product)
    private readonly repoProduct: Repository<Product>
  ) {}

  getAll() {
    return this.repoRedeem.find();
  }

  async save(req: { user: number; product: number }) {
    const options: FindOneOptions<Users> = {
      relations: ["point", "redeem"],
      where: { id: req.user },
    };

    const _cuser = await this.repoUser.findOne(options);

    if (!_cuser) {
      errorMessage("User not found");
    }

    const _cproduct = await this.repoProduct.findOne(req.product);

    if (!_cproduct) {
      errorMessage("Product not found");
    }

    if (_cuser.totalPoint < _cproduct.redeemPoint) {
      errorMessage("Point not enough");
    }

    const redeem = new Redeem();

    redeem.point = _cproduct.redeemPoint;
    redeem.product = _cproduct;
    redeem.user = _cuser;

    return this.repoRedeem.save(redeem);
  }
}
