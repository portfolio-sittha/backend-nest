import { IsNotEmpty } from "class-validator"

export class RedeemSaveDto {
  @IsNotEmpty()
  user: number

  @IsNotEmpty()
  product: number
}