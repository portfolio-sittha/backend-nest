import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { FindManyOptions, Repository, UpdateResult } from "typeorm";
import { Users } from "../enitites/users.enitity";
import { Bill } from "../enitites/bill.enitity";
import { Redeem } from "../enitites/redeem.enitity.";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private readonly repoUsers: Repository<Users>,
    @InjectRepository(Bill)
    private readonly repoBill: Repository<Bill>,
    @InjectRepository(Redeem)
    private readonly repoRedeem: Repository<Redeem>
  ) {}

  async getAll(): Promise<Users[]> {
    return this.repoUsers.find();
  }

  async get(req: { userId: string }): Promise<Users[]> {
    const options: FindManyOptions<Users> = {
      where: [{ id: req.userId }],
      relations: ["bill", "point", "redeem", "redeem.product"],
    };

    return this.repoUsers.find(options);
  }

  async checkDuplicate(email: string): Promise<number> {
    return this.repoUsers.count({ where: [{ primaryEmail: email }] });
  }

  async save(email: string, password: string, phone: string): Promise<Users> {
    const user = new Users();

    user.primaryEmail = email;
    user.password = password;
    user.phone = phone;

    return this.repoUsers.save(user);
  }

  async update(id: string, password: string, phone: string): Promise<Users> {
    const user = await this.repoUsers.findOne(id);

    user.phone = phone;
    user.password = password;

    return this.repoUsers.save(user);
  }

  async delete(id: string): Promise<{ result: UpdateResult; err: string; }> {
    const user = await this.repoUsers.findOne(id);

    if (!user) {
      return { result: null, err: "can't find users" };
    }

    const result = await this.repoUsers.softDelete(id)

    return { result: result, err: null };
  }
}
