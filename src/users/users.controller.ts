import { Body, Controller, Delete, Get, Param, Post, Put, Res } from "@nestjs/common";
import { Response } from "express";
import { UserSaveDto, UserUpdateDto } from "./dto/user-save.dto";
import { UsersService } from "./users.service";
import { errorMessage } from "../lib/general";

@Controller("/users")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get() async getUser(@Res() res: Response) {
    const result = await this.usersService.getAll();

    res.send({ result: result });
  }

  @Get(":id") async getBill(@Param("id") id: string, @Res() res: Response) {
    const r = await this.usersService.get({ userId: id });

    res.send(r);
  }

  @Post() async saveUser(@Body() req: UserSaveDto, @Res() res: Response) {
    const c = await this.usersService.checkDuplicate(req.email);

    if (c > 0) {
      errorMessage("User is Duplicate");
    }

    const r = await this.usersService.save(req.email, req.password, req.phone);

    res.send({ result: r });
  }

  @Put(":id") async updateUser(@Param("id") id: string, @Body() req: UserUpdateDto, @Res() res: Response) {
    const r = await this.usersService.update(id, req.password, req.phone);

    res.send({ result: r });
  }

  @Delete(":id") async deleteUser(@Param("id") id: string, @Res() res: Response) {
    const { result, err } = await this.usersService.delete(id);

    if (err) {
      res.send({ result: err });
      return;
    }

    res.send({ result: "ok" });
  }
}
