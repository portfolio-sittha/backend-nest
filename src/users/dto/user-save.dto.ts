import { IsNotEmpty } from "class-validator";
export class UserGetDto {
  id: string
}


export class UserSaveDto {
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  phone: string;
}


export class UserUpdateDto {
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  phone: string;
}

