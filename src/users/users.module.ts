import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";
import { Users } from "../enitites/users.enitity";
import { Bill } from "src/enitites/bill.enitity";
import { Redeem } from "src/enitites/redeem.enitity.";

@Module({
  imports: [TypeOrmModule.forFeature([Users, Bill, Redeem])],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
