import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Bill } from "src/enitites/bill.enitity";
import { Users } from "src/enitites/users.enitity";
import { Repository } from "typeorm";
import { errorMessage } from '../lib/general';

@Injectable()
export class BillService {
  constructor(
    @InjectRepository(Bill)
    private readonly repositoryBill: Repository<Bill>,
    @InjectRepository(Users)
    private readonly repositoryUser: Repository<Users>
  ) {}

  async getAll() {
    return this.repositoryBill.find();
  }

  async get(req: { id: any }) {
    return this.repositoryBill.findOne(req.id);
  }

  async save(req: { userId: any }) {
    const user = await this.repositoryUser.findOne(req.userId);

    if (!user) {
      errorMessage("User not found")
    }

    const bill = new Bill();
    bill.user = user;

    return this.repositoryBill.save(bill);
  }
}
