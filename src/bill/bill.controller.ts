import { Controller, Get, Post } from "@nestjs/common";
import { Body, Res } from "@nestjs/common/decorators/http/route-params.decorator";
import { Response } from "express";
import { BillService } from "./bill.service";
import { BillSave } from "./dto/bill.dto";

@Controller("/bill")
export class BillController {
  constructor(private readonly billService: BillService) {}

  @Get() async getAll(@Res() res: Response) {
    const result = await this.billService.getAll();

    res.send({ result: result });
  }

  @Post() async save(@Body() req: BillSave, @Res() res: Response) {
    const result = await this.billService.save({ userId: req.user });

    res.send({ bill: result });
  }
}
