import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Bill } from "src/enitites/bill.enitity";
import { Users } from "src/enitites/users.enitity";
import { BillController } from "./bill.controller";
import { BillService } from "./bill.service";

@Module({
  imports: [TypeOrmModule.forFeature([Users, Bill])],
  controllers: [BillController],
  providers: [BillService],
})
export class BillModule {}
