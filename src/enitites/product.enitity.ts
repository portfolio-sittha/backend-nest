import { classToPlain, Exclude, Expose, Transform } from "class-transformer";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Product {
  public constructor(init?: Partial<Product>) {
    Object.assign(this, init);
  }

  @PrimaryGeneratedColumn({ type: "integer", unsigned: true })
  id!: number;

  @Column({ type: "varchar" })
  name: string;

  @Column({ type: "integer", unsigned: true })
  redeemPoint: number;

  @Exclude({ toPlainOnly: true })
  @CreateDateColumn()
  createAt: Date;

  @Exclude({ toPlainOnly: true })
  @UpdateDateColumn()
  updatedAt: Date;

  @Expose({ name: "isDelete" })
  @Transform((value) => (value !== null ? true : false))
  @DeleteDateColumn()
  deletedAt: Date;

  toJSON() {
    return classToPlain(this);
  }
}
