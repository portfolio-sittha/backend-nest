import { classToPlain, Exclude, Expose, Transform } from "class-transformer";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  OneToMany,
  AfterLoad,
} from "typeorm";
import { Bill } from "./bill.enitity";
import { Point } from "./point.enitity";
import { Redeem } from "./redeem.enitity.";

@Entity()
export class Users {
  public constructor(init?: Partial<Users>) {
    Object.assign(this, init);
  }

  @PrimaryGeneratedColumn({ type: "integer", unsigned: true })
  id!: number;

  @Column({ type: "varchar", length: 100 })
  primaryEmail: string;

  @Column({ type: "varchar", length: 12 })
  phone: string;

  @Exclude({ toPlainOnly: true })
  @Column({ type: "varchar", length: 100 })
  password: string;

  @OneToMany(() => Bill, (bill) => bill.user, {
    onDelete: "CASCADE",
    onUpdate: "NO ACTION",
  })
  bill: Bill[];

  @Exclude({ toPlainOnly: true })
  @OneToMany(() => Point, (point) => point.user, {
    onDelete: "CASCADE",
    onUpdate: "NO ACTION",
  })
  point: Point[];

  totalPoint: number;

  // @Exclude({ toPlainOnly: true })
  @OneToMany(() => Redeem, (redeem) => redeem.user, {
    onDelete: "CASCADE",
    onUpdate: "NO ACTION",
  })
  redeem: Redeem[];

  totalRedeem: number;

  @Exclude({ toPlainOnly: true })
  @CreateDateColumn()
  createAt: Date;

  @Exclude({ toPlainOnly: true })
  @UpdateDateColumn()
  updatedAt: Date;

  @Expose({ name: "isDelete" })
  @Transform((value) => (value !== null ? true : false))
  @DeleteDateColumn()
  deletedAt: Date;

  toJSON() {
    return classToPlain(this);
  }

  @AfterLoad()
  setTotal() {
    if (this.redeem) {
      let total = 0;

      for (const r of this.redeem) {
        total = total + r.point;
      }

      this.totalRedeem = total;
    }

    if (this.point) {
      let total = 0;

      for (const p of this.point) {
        total = total + p.point;
      }

      this.totalPoint = total - this.totalRedeem;
    }
  }
}
