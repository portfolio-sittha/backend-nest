import { classToPlain, Exclude, Expose, Transform } from "class-transformer";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, DeleteDateColumn, UpdateDateColumn, JoinColumn, ManyToOne } from "typeorm";
import { Bill } from './bill.enitity';
import { Product } from "./product.enitity";
import { Users } from "./users.enitity";

@Entity()
export class Redeem {
  public constructor(init?: Partial<Redeem>) {
    Object.assign(this, init);
  }

  @PrimaryGeneratedColumn({ type: "integer", unsigned: true })
  id!: number;

  @JoinColumn({ name: 'product_id', referencedColumnName: 'id' })
  @ManyToOne(
    () => Product,
    product => product.id,
    {
      onDelete: 'CASCADE',
      onUpdate: 'NO ACTION',
      primary: true
    }
  )
  product: Product;

  @JoinColumn({ name: "user_id", referencedColumnName: "id" })
  @ManyToOne(() => Users, (user) => user.id, {
    onDelete: "CASCADE",
    onUpdate: "NO ACTION",
    primary: true
  })
  user: Users;

  @Column({ type: "integer", unsigned: true })
  point: number;

  @Exclude({ toPlainOnly: true })
  @CreateDateColumn()
  createAt: Date;

  @Exclude({ toPlainOnly: true })
  @UpdateDateColumn()
  updatedAt: Date;

  @Expose({ name: "isDelete" })
  @Transform((value) => (value !== null ? true : false))
  @DeleteDateColumn()
  deletedAt: Date;

  toJSON() {
    return classToPlain(this);
  }
}
