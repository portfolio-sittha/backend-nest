import { classToPlain, Exclude, Expose, Transform } from "class-transformer";
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  JoinColumn,
  ManyToOne,
} from "typeorm";
import { Users } from "./users.enitity";

@Entity()
export class Bill {
  public constructor(init?: Partial<Bill>) {
    Object.assign(this, init);
  }

  @PrimaryGeneratedColumn({ type: "integer", unsigned: true })
  id!: number;

  @JoinColumn({ name: "user_id", referencedColumnName: "id" })
  @ManyToOne(() => Users, (user) => user.id, {
    onDelete: "CASCADE",
    onUpdate: "NO ACTION",
    primary: true
  })
  user: Users;

  @Exclude({ toPlainOnly: true })
  @CreateDateColumn()
  createAt: Date;

  @Exclude({ toPlainOnly: true })
  @UpdateDateColumn()
  updatedAt: Date;

  @Expose({ name: "isDelete" })
  @Transform((value) => (value !== null ? true : false))
  @DeleteDateColumn()
  deletedAt: Date;

  toJSON() {
    return classToPlain(this);
  }
}
