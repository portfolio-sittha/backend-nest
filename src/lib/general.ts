import { HttpException, HttpStatus } from "@nestjs/common";

export const errorMessage = (message: string) => {
  throw new HttpException(
    {
      status: HttpStatus.FORBIDDEN,
      error: message,
    },
    HttpStatus.FORBIDDEN
  );
};
