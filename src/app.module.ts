import { Module } from "@nestjs/common";
import { TypeOrmModule, TypeOrmModuleOptions } from "@nestjs/typeorm";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import * as Enitites from "./enitites";
import { UsersModule } from "./users/users.module";
import { BillModule } from "./bill/bill.module";
import { PointModule } from "./point/point.module";
import { ProductModule } from "./product/product.module";
import { RedeemModule } from "./redeem/redeem.module";

const LIST: any[] = Object.keys(Enitites).map((key) => Enitites[key]);

export const DATABASE: TypeOrmModuleOptions = {
  type: "mysql",
  host: "34.87.119.17",
  port: 3306,
  username: "backendNest",
  password: "rq9mtpfyjmedv77rdy3h7uhm",
  database: "backendNest",
  retryDelay: 3000,
  retryAttempts: 1,
  autoLoadEntities: true,
  synchronize: true,
  namingStrategy: new SnakeNamingStrategy(),
  entities: LIST,
};

@Module({
  imports: [TypeOrmModule.forRoot(DATABASE), UsersModule, BillModule, PointModule, ProductModule, RedeemModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
