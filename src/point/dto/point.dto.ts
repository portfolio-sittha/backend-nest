import { IsNotEmpty, Min } from "class-validator";

export class PointSaveDto {
  @IsNotEmpty()
  user: number;

  @IsNotEmpty()
  bill: number;

  @Min(0)
  point: number;
}
