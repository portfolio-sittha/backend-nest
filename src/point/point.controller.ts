import { Body, Controller, Get, Post, Res } from "@nestjs/common";
import { Response } from "express";
import { PointSaveDto } from "./dto/point.dto";
import { PointService } from "./point.service";

@Controller("/point")
export class PointController {
  constructor(private readonly pointService: PointService) {}

  @Get() getAll() {}

  @Post() async save(@Body() req: PointSaveDto, @Res() res: Response) {
    const result = await this.pointService.save({ userId: req.user, billId: req.bill, point: req.point });

    res.send({ bill: result });
  }
}
