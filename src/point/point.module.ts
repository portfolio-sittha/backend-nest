import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Point } from "src/enitites/point.enitity";
import { PointController } from "./point.controller";
import { PointService } from "./point.service";
import { Users } from '../enitites/users.enitity';
import { Bill } from '../enitites/bill.enitity';

@Module({
  imports: [TypeOrmModule.forFeature([Point, Users, Bill])],
  controllers: [PointController],
  providers: [PointService],
})
export class PointModule {}
