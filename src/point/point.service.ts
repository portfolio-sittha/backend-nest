import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Users, Bill } from "src/enitites";
import { Repository } from "typeorm";
import { Point } from "../enitites/point.enitity";
import { errorMessage } from "../lib/general";

@Injectable()
export class PointService {
  constructor(
    @InjectRepository(Point)
    private readonly repositoryPoint: Repository<Point>,
    @InjectRepository(Users)
    private readonly repositoryUser: Repository<Users>,
    @InjectRepository(Bill)
    private readonly repositoryBill: Repository<Bill>
  ) {}

  async save(req: { userId: any; billId: any; point: number }) {
    const user = await this.repositoryUser.findOne(req.userId);

    if (!user) {
      errorMessage("User not found");
    }

    const bill = await this.repositoryBill.findOne({ id: req.billId });

    if (!bill) {
      errorMessage("Bill not found");
    }

    const point = new Point();

    point.bill = bill;
    point.user = user;
    point.point = req.point;

    return this.repositoryPoint.save(point);
  }
}
