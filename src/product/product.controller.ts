import { Body, Controller, Get, Post, Res } from "@nestjs/common";
import { Response } from "express";
import { ProductSaveDto } from "./dto/product.dto";
import { ProductService } from "./product.service";

@Controller("/product")
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get() async getAll(@Res() res: Response) {
    const result = await this.productService.getAll();
    res.send({ result: result });
  }

  @Post() async save(@Body() req: ProductSaveDto, @Res() res: Response) {
    const result = await this.productService.save({ name: req.name, redeemPoint: req.redeemPoint });
    res.send({ result: result });
  }
}
