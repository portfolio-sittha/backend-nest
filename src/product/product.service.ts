import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Product } from "src/enitites/product.enitity";
import { Repository } from "typeorm";

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly repositoryProduct: Repository<Product>
  ) {}

  getAll() {
    return this.repositoryProduct.find();
  }

  save(req: {name: string, redeemPoint: number}) {
    const product = new Product

    product.name = req.name
    product.redeemPoint = req.redeemPoint

    return this.repositoryProduct.save(product)
  }
}
