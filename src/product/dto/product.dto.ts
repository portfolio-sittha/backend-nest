import { IsNotEmpty, Min } from "class-validator";

export class ProductSaveDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Min(0)
  redeemPoint: number;
}